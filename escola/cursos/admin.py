from django.contrib import admin

from escola.cursos.models import Course, Avaliation


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'url', 'created_at', 'updated_at')
    list_display_links = ('id', 'title')
    search_fields = ('id', 'title')
    list_filter = ('created_at', 'updated_at')


@admin.register(Avaliation)
class AvaliationAdmin(admin.ModelAdmin):
    list_display = ('id', 'course', 'name', 'note', 'created_at', 'updated_at')
    list_display_links = ('id', 'name')
    search_fields = ('course_id', 'name', 'note')
    list_filter = ('created_at', 'updated_at')

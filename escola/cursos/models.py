from django.db import models


class AbstractModel(models.Model):
    created_at = models.DateTimeField(verbose_name='Criado', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='Atualizado', auto_now=True)
    active = models.BooleanField(verbose_name='Ativo?', default=True)

    class Meta:
        abstract = True


class Course(AbstractModel):
    title = models.CharField(verbose_name='Título', max_length=100)
    url = models.URLField(verbose_name='Url', unique=True)

    class Meta:
        verbose_name = 'Curso'
        verbose_name_plural = 'Cursos'

    def __str__(self):
        return self.title


class Avaliation(AbstractModel):
    course = models.ForeignKey(Course, verbose_name='Curso', related_name='avaliations', on_delete=models.DO_NOTHING)
    name = models.CharField(verbose_name='Nome', max_length=100)
    email = models.EmailField(verbose_name='Email')
    commentary = models.TextField(verbose_name='Comentário', blank=True, default='')
    note = models.DecimalField(verbose_name='Nota', max_digits=2, decimal_places=1)

    class Meta:
        verbose_name = 'Avaliação'
        verbose_name_plural = 'Avaliações'
        unique_together = ['email', 'course']

    def __str__(self) -> str:
        return f'{self.name} avaliaou o curso {self.course} com nota {self.note}'

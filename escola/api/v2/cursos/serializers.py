from rest_framework import serializers

from escola.cursos.models import Avaliation, Course


class AvaliationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Avaliation
        fields = (
            'id',
            'course',
            'name',
            'email',
            'commentary',
            'note',
            'created_at',
            'active',
        )
        extra_kwargs = {'email': {'write_only': True}}


class CourseSerializer(serializers.ModelSerializer):
    # Nested Relationship
    avaliations = AvaliationSerializer(many=True, read_only=True)

    # HyperLinked Related Field
    # avaliations = serializers.HyperlinkedRelatedField(many=True, read_only=True, view_name='avaliations-detail')

    # PrimaryKey Related Field
    # avaliations = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Course
        fields = ('id', 'title', 'url', 'created_at', 'active', 'avaliations')

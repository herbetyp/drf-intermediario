from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from rest_framework.response import Response

from escola.cursos.models import Course, Avaliation
from escola.api.v2.cursos.serializers import CourseSerializer, AvaliationSerializer


class CoursesViewSet(ModelViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer

    @action(detail=True, methods=['GET'])
    def avaliacoes(self, request, pk=None):
        course = self.get_object()
        serializer = AvaliationSerializer(course.avaliations.all(), many=True)

        return Response(serializer.data)


class AvaliationsViewSet(ModelViewSet):
    queryset = Avaliation.objects.all()
    serializer_class = AvaliationSerializer

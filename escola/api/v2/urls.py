from rest_framework.routers import SimpleRouter

from escola.api.v2.cursos.viewsets import CoursesViewSet, AvaliationsViewSet


router = SimpleRouter()
router.register('cursos', CoursesViewSet)
router.register('avaliacoes', AvaliationsViewSet)

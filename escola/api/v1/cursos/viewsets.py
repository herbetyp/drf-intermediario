from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView, get_object_or_404

from escola.cursos.models import Course, Avaliation
from escola.api.v1.cursos.serializers import CourseSerializer, AvaliationSerializer


class CoursesViewSet(ListCreateAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class CourseDetailViewSet(RetrieveUpdateDestroyAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class AvaliationsViewSet(ListCreateAPIView):
    queryset = Avaliation.objects.all()
    serializer_class = AvaliationSerializer

    def get_queryset(self):
        course_pk = self.kwargs.get('cs_pk')

        if course_pk:
            return self.queryset.filter(course_id=course_pk)
        return self.queryset.all()


class AvaliationDetailViewSet(RetrieveUpdateDestroyAPIView):
    queryset = Avaliation.objects.all()
    serializer_class = AvaliationSerializer

    def get_object(self):
        course_pk = self.kwargs.get('cs_pk')
        avaliation_pk = self.kwargs.get('av_pk')

        if course_pk:
            return get_object_or_404(self.get_queryset(), course_id=course_pk, pk=avaliation_pk)
        return get_object_or_404(self.get_queryset(), pk=avaliation_pk)

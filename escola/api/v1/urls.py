from django.urls import path

from escola.api.v1.cursos.viewsets import (
    CoursesViewSet,
    CourseDetailViewSet,
    AvaliationsViewSet,
    AvaliationDetailViewSet,
)


app_name = 'courses'

urlpatterns = [
    path('cursos', CoursesViewSet.as_view(), name='courses'),
    path('cursos/<int:pk>', CourseDetailViewSet.as_view(), name='course_detail'),
    path('cursos/<int:cs_pk>/avaliacoes', AvaliationsViewSet.as_view(), name='course_avaliations'),
    path('cursos/<int:cs_pk>/avaliacoes/<int:av_pk>/', AvaliationDetailViewSet.as_view(), name='course_avaliation'),
    path('avaliacoes', AvaliationsViewSet.as_view(), name='avaliations'),
    path('avaliacoes/<int:av_pk>', AvaliationDetailViewSet.as_view(), name='avaliation_detail'),
]
